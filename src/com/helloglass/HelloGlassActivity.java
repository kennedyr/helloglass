package com.helloglass;

import com.google.android.glass.app.Card;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;

public class HelloGlassActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		Card helloWorldCard = new Card(this);
		helloWorldCard.setText("Hello Glass!");
		View helloWorldView = helloWorldCard.getView();
		setContentView(helloWorldView);
	}
}
