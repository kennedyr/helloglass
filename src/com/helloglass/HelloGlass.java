package com.helloglass;

import com.google.android.glass.timeline.LiveCard;
import android.os.IBinder;
import android.app.Service;
import android.content.Intent;

public class HelloGlass extends Service {
	private static final String LIVE_CARD_ID = "helloglass";
	
	@SuppressWarnings("unused")
	private LiveCard mLiveCard;
	
	@Override
	public void onCreate(){
		super.onCreate();
		
	}
	
	@Override
	public IBinder onBind(Intent intent){
		return null;
	}
	
	public int onStartCommand(Intent intent, int flags, int startId){
		mLiveCard = new LiveCard(this, LIVE_CARD_ID);
		Intent i = new Intent(this, HelloGlassActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
		return START_STICKY;
	}
	
}